$("#exampleModal").modal("show");
  
function onClick() {
  let password = document.getElementById("password").value;
  let title = document.getElementById("title").innerHTML;
  let content = document.getElementById("content").innerHTML;

  title = CryptoJS.AES.decrypt(title, password);
  content = CryptoJS.AES.decrypt(content, password);

  title = title.toString(CryptoJS.enc.Utf8)
  content = content.toString(CryptoJS.enc.Utf8)

  // Unable to decrypt
  if (title == '') {
    location.reload();
  }

  if (title != null) {
    document.getElementById("title").innerHTML = title;
    document.getElementById("title").hidden = false;
    document.getElementById("content").innerHTML = content;
    document.getElementById("content").hidden = false;
    document.getElementById("flag").hidden = false;
    
  } 
  
}

